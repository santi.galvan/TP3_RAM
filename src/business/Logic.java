package business;

import java.util.ArrayList;

public class Logic 
{
	private Solver solver;	
	private Squad squad;
	private Incongruity inconsistencies;
	private Scheme scheme;
	private Formation solution;
	
	public Logic()
	{
		squad = new Squad();
		inconsistencies = new Incongruity(); 
	}
	
	public void execute(String type) 
	{
		if(type.equals("Backtraking"))
		{
			solver = new Backtracking(squad, inconsistencies, scheme.getAlignament());
			solution = solver.solve();
			System.out.println("BAK");
		}
		else if(type.equals("Fuerza bruta"))
		{
			solver  = new BruteForce(squad, inconsistencies, scheme.getAlignament());
			solution = solver.solve();
			System.out.println("BF");
		}
		else if(type.equals("Heuristica Exponencial"))
		{
			solver = new HeuristicExponential(squad, inconsistencies, scheme.getAlignament());
			solution = solver.solve();
			System.out.println("HEURISTICA EXP");
		}
		
		else if(type.equals("Heuristica Golosa"))
		{
			solver = HeuristicPolinomy.complete(squad, inconsistencies, scheme.getAlignament());
			solution = solver.solve();
			System.out.println("HEURISTICA GOL");
		}
	} 
	
	public ArrayList<Formation> executeAllInThreads()
	{
		ArrayList<Formation> ret = new ArrayList<Formation>();
		
		Solver bruteforce = new BruteForce(squad, inconsistencies, scheme.getAlignament());
		Solver backtraking  = new Backtracking(squad, inconsistencies, scheme.getAlignament());
		Solver heuristicExponential = new HeuristicExponential(squad, inconsistencies, scheme.getAlignament());

		bruteforce.run();
		backtraking.run();
		heuristicExponential.run();
		
		ret.add(bruteforce.getIncumbent());
		ret.add(backtraking.getIncumbent());
		ret.add(heuristicExponential.getIncumbent());
		
		return ret;
	}
	
	public boolean loadPlayer(Player p) 
	{
		return squad.addPlayer(p);
	}
	
	public void addIncongruity(Player one, Player two)
	{
		inconsistencies.addIncongruity(one, two);
	}
	
	public void setScheme(String aligment) 
	{
		scheme = new Scheme(aligment);
	}
	
	public double punctuationFormation()
	{
		return solution.appraiseTeam();
	}

	public ArrayList<String> solutionInArray() 
	{
		ArrayList<String> ret = new ArrayList<>();
		solution.getPlayers().stream().forEach(e -> ret.add(e.getName()+","+e.getLevel()+","+e.getPosition()));
		return ret;
	}
	
	public Squad getSquad()
	{
		return squad;
	}

}
	
