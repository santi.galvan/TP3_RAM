package business;

import static org.junit.Assert.*;

import org.junit.Test;

import business.Player.Position;

public class FormationTest {

	@Test
	public void punctuationTest()
	{
		Formation instance = titularArgentina("4-4-2");
		
		assertEquals(90, instance.appraiseTeam(), 0.0);
	}
	
	@Test
	public void respectSchemeTest()
	{
		Formation instance = titularArgentina("3-5-2");
		
		instance.add(new Player("Higuain",9,Position.Striker));
		
		assertFalse(instance.respectScheme());
	}
	
	@Test
	public void lackStrikerTest()
	{
		Formation instance = Utilities.createFormation("3-5-2",new String[] { 
				"Romero     :  9  (GoalKeeper)", 
				
				"Pezzela    :  8  (RightBack)",
				"Mascherano :  9  (LeftBack)", 
				"Mercado    :  8  (CentralDefender)", 
				
				"Acuna      :  6  (RightWinger)",
				"Paredes    :  7  (MidFielder)", 
				"I.Fernandez:  7  (MidFielder)",
				"E.Perez    :  9  (RightWinger)",
				"G.Martinez :  9  (RightWinger)",
				
				"Messi      :  10 (Striker)"});
		
		Player higuian = new Player("Higuain",9,Position.Striker);
		
		assertTrue((instance.allow(higuian)));
	}
	
	@Test
	public void fullStrikerTest()
	{
		Formation instance = titularArgentina("3-5-2");
		Player higuian = new Player("Higuain",9,Position.Striker);
		
		assertFalse((instance.allow(higuian)));
	}
	
	@Test
	public void isBetterTest()
	{
		Formation less = suplentArgentina("3-5-2");
		Formation higher = titularArgentina("3-5-2");
		
		assertTrue(higher.isBetter(less));
	}
	
	public Formation titularArgentina(String alignment)
	{
		Formation formation = Utilities.createFormation(alignment,new String[] { 
				"Romero     :  9  (GoalKeeper)", 
				
				"Pezzela    :  8  (RightBack)",
				"Mascherano :  9  (LeftBack)", 
				"Mercado    :  8  (CentralDefender)", 
				
				"Acuna      :  6  (RightWinger)",
				"Paredes    :  7  (MidFielder)", 
				"I.Fernandez:  7  (MidFielder)",
				"E.Perez    :  9  (RightWinger)",
				"G.Martinez :  9  (RightWinger)",
				
				"Messi      :  10 (Striker)",
				"Icardi     :  8  (Striker)"});
		
		return formation;
	}
	
	public Formation suplentArgentina(String alignment)
	{
		Formation formation = Utilities.createFormation(alignment,new String[] { 
				"Guzman     :  8  (GoalKeeper)",
				
			    "Fazio	    :  5  (CentralDefender)", 
				"Mammana    :  7  (LeftBack)", 
				"Otamendi   :  7  (CentralDefender)",
				
				"Di Maria   :  7  (LeftWinger)",
				"Biglia     :  6  (RightWinger)", 
				"P.Perez    :  7  (MidFielder)", 
				"Gago       :  8  (MidFielder)",
				"Banega     :  8  (MidFielder)",
				
				"Benedeto   :  7  (Striker)", 
				"Dybala     :  9  (Striker)"});
		
		return formation;
	}
	
}
