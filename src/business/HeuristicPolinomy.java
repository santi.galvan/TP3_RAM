package business;

import java.util.Collections;

public class HeuristicPolinomy extends Solver
{
	private Formation solution;
	
	public static HeuristicPolinomy complete(Squad instance, Incongruity incongruity, String aligment)
	{
		return new HeuristicPolinomy(instance, incongruity, aligment);
	}
	
	public static HeuristicPolinomy simple(Squad instance, String aligment)
	{
		return new HeuristicPolinomy(instance, new Incongruity(), aligment);
	}
	
	private HeuristicPolinomy(Squad instance, Incongruity incongruity, String aligment) 
	{
		this.instance = instance;
		this.inconsistencies = incongruity;
		this.alignment = new Scheme(aligment);
	}
	
	public Formation solve()
	{
		sort();
		solution = new Formation(alignment);
		for(Player player: instance.squad())
		{
			if(bringIn(player, solution)) 
					solution.add(player);
		}
		 //numero magico, poner algo que haga referencia a ese 11
		return solution.respectScheme() ? solution : new Formation(alignment);
	}

	private boolean bringIn(Player player, Formation formation)
	{
		return formation.allow(player) && !inconsistencies.existIncongruity(player, formation.getPlayers());
	}
	
	private void sort() 
	{
		Collections.sort(instance.squad(), PlayerComparator.byLevel());
	}
	
}
