package business;

public class BruteForce extends Solver
{
	private Formation solution;
	
	public BruteForce(Squad instance, Incongruity incongruity, String alignment) 
	{
		this.instance = instance;
		this.inconsistencies = incongruity;
		this.alignment = new Scheme(alignment); 
	}
	
	@Override
	Formation solve()
	{
		super.startStadistics();	
		incumbent = new Formation(alignment);
		
		bruteForce();
		
		super.endStadistics();
		System.out.println("Tiempo: "+time()+", Iteraciones: "+iteraciones+", Cambio de solucion: "+cambioDeSoluciones);
		
		return incumbent;	
	}
	
	private void bruteForce()
	{
		solution = new Formation(alignment);
		generateFrom(0);
	}
	
	private void generateFrom(int i) 
	{
		iteraciones++;
		if(i == instance.numberOfplayers())
		{
			if(isFeasible(solution) && solution.isBetter(incumbent)) 
				incumbent = solution.clone();
		}
		else 
		{
			solution.add(instance.getPlayer(i));
			generateFrom(i+1);
				
			solution.delete(instance.getPlayer(i));
			generateFrom(i+1);
		}
	}

	private boolean isFeasible(Formation candidate) 
	{
		return candidate.respectScheme() && !inconsistencies.inTeam(candidate.getPlayers());
	}
	
}
