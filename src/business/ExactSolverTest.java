package business;

import org.junit.Test;

public class ExactSolverTest 
{
	@Test(expected = IllegalArgumentException.class)
	public void noTeamTest()
	{
		Squad squad = Utilities.createSquad(new String[] { });
		
		Formation expected = Utilities.createFormation("4-3-3", new String[] { });
		
		checkSolution(squad, expected, new Incongruity());
	}

	@Test
	public void happyPathTest()
	{
		Squad squad = Utilities.createSquad(new String[] { 
				"Guzman     :  8  (GoalKeeper)",
				"Romero     :  9  (GoalKeeper)", 
			    "Marchesin  :  4  (GoalKeeper)",

				"Fazio	    :  5  (CentralDefender)", 
				"Mammana    :  7  (LeftBack)", 
				"Pezzela    :  8  (RightBack)",
				"Mascherano :  9  (LeftBack)", 
				"Otamendi   :  7  (CentralDefender)",
				"Mercado    :  8  (CentralDefender)", 

				"Acuna      :  6  (RightWinger)",
				"Di Maria   :  7  (LeftWinger)",
				"Biglia     :  6  (RightWinger)", 
				"Paredes    :  7  (MidFielder)", 
				"Banega     :  8  (MidFielder)",
				"P.Perez    :  7  (MidFielder)", 
				"Gago       :  8  (MidFielder)",
				"E.Perez    :  9  (RightWinger)",
					
				"Messi      :  10 (Striker)",
				"Benedeto   :  7  (Striker)", 
				"Icardi     :  8  (Striker)", 
				"Dybala     :  9  (Striker)"});
		
		Formation expected = Utilities.createFormation("4-3-3", new String[] {
				"Romero     :  9  (GoalKeeper)", 

				"Otamendi   :  7  (CentralDefender)",
				"Pezzela    :  8  (RightBack)",
				"Mercado    :  8  (CentralDefender)", 
				"Mascherano :  9  (LeftBack)", 

				"Banega     :  8  (MidFielder)",
				"Gago       :  8  (MidFielder)",
				"E.Perez    :  9  (RightWinger)",
				
				"Icardi     :  8  (Striker)", 
				"Dybala     :  9  (Striker)",
				"Messi      :  10 (Striker)" });
		
			
		checkSolution(squad, expected, new Incongruity());
	}
	
	@Test
	public void inconsistenciesTest()
	{
		Squad squad = Utilities.createSquad(new String[] { 
				"Guzman     :  8  (GoalKeeper)",
				"Romero     :  10 (GoalKeeper)", 
			    "Marchesin  :  4  (GoalKeeper)",

				"Fazio	    :  4  (CentralDefender)", 
				"Mammana    :  5  (LeftBack)", 
				"Pezzela    :  8  (RightBack)",
				"Mascherano :  10  (LeftBack)", 
				"Otamendi   :  7  (CentralDefender)",
				"Mercado    :  8  (CentralDefender)", 

				"Acuna      :  6  (RightWinger)",
				"Di Maria   :  7  (LeftWinger)",
				"Biglia     :  6  (RightWinger)", 
				"Paredes    :  7  (MidFielder)", 
				"Banega     :  8  (MidFielder)",
				"P.Perez    :  7  (MidFielder)", 
				"Gago       :  8  (MidFielder)",
				"E.Perez    :  9  (RightWinger)",
					
				"Messi      :  10 (Striker)",
				"Benedeto   :  7  (Striker)", 
				"Icardi     :  10 (Striker)", 
				"Dybala     :  9  (Striker)"});
		
		Formation expected = Utilities.createFormation("4-3-3", new String[] {
				"Guzman     :  8  (GoalKeeper)", 

				"Otamendi   :  7  (CentralDefender)",
				"Pezzela    :  8  (RightBack)",
				"Mercado    :  8  (CentralDefender)", 
				"Mascherano :  10  (LeftBack)", 
				
				"Banega     :  8  (MidFielder)",
				"Gago       :  8  (MidFielder)",
				"E.Perez    :  9  (RightWinger)",
				
				"Icardi     :  10 (Striker)", 
				"Dybala     :  9  (Striker)", 
				"Messi      :  10 (Striker)" });
		
		
		Incongruity inconsistencies = Utilities.incongruityBetween(new String[] {
				"Romero : 10 (GoalKeeper) <> Messi : 10 (Striker)"});
			
		checkSolution(squad, expected, inconsistencies);
	}
	
	@Test
	public void unounounoTest()
	{
		Squad squad = Utilities.createSquad(new String[] { 
				"Romero     :  10 (GoalKeeper)", 
				"Fazio	    :  4  (CentralDefender)", 
				"E.Perez    :  9  (RightWinger)",
				"Dybala     :  9  (Striker)"});
		
		Formation expected = Utilities.createFormation("1-1-1", new String[] {
				"Romero     :  10 (GoalKeeper)", 
				"Fazio	    :  4  (CentralDefender)", 
				"E.Perez    :  9  (RightWinger)",
				"Dybala     :  9  (Striker)"});
		
		checkSolution(squad, expected, new Incongruity());
	}
	
	@Test
	public void fullInconsistenciesTest()
	{
		Squad squad = Utilities.createSquad(new String[] { 
				"Guzman     :  8  (GoalKeeper)",
				"Romero     :  10 (GoalKeeper)", 
			    "Marchesin  :  4  (GoalKeeper)",

				"Fazio	    :  4  (CentralDefender)", 
				"Mammana    :  5  (LeftBack)", 
				"Pezzela    :  8  (RightBack)",
				"Mascherano :  10  (LeftBack)", 
				"Otamendi   :  7  (CentralDefender)",
				"Mercado    :  8  (CentralDefender)", 

				"Acuna      :  6  (RightWinger)",
				"Di Maria   :  7  (LeftWinger)",
				"Biglia     :  6  (RightWinger)", 
				"Paredes    :  7  (MidFielder)", 
				"Banega     :  8  (MidFielder)",
				"P.Perez    :  7  (MidFielder)", 
				"Gago       :  8  (MidFielder)",
				"E.Perez    :  9  (RightWinger)",
					
				"Messi      :  10 (Striker)",
				"Benedeto   :  7  (Striker)", 
				"Icardi     :  10 (Striker)", 
				"Dybala     :  9  (Striker)"});
		
		Formation expected = Utilities.createFormation("4-3-3", new String[] { });
		
		Incongruity inconsistencies = Utilities.fullIncongruity(squad);
			
		checkSolution(squad, expected, inconsistencies);
	}
	
	private void checkSolution(Squad instance, Formation expected, Incongruity inconsistencies) 
	{
		Solver backtraking = new Backtracking(instance, inconsistencies, expected.getScheme());
		Formation BTSolution = backtraking.solve();
		
		Solver bruteforce = new BruteForce(instance, inconsistencies, expected.getScheme());
		Formation FBSolution = bruteforce.solve();
		
		Utilities.samePunctuation(expected, BTSolution);
		Utilities.samePunctuation(expected, FBSolution);
	}

}
