package business;

import java.util.ArrayList;

public class HeuristicExponential extends Solver
{
	private ArrayList<ArrayList<Combination<Player>>> combinations;
	
	public HeuristicExponential(Squad instance, Incongruity inconsistencies, String schemeSystem) 
	{
		super.instance = instance;
		super.inconsistencies = inconsistencies;
		super.alignment = new Scheme(schemeSystem); 
	}
	
	@Override
	public Formation solve() 
	{
		super.startStadistics();	
		
		incumbent = new Formation(alignment);
		
		this.combinations = CombinationAlgorithm.combinationsTakenFrom(instance.separateSquadByPosition(), alignment.getScheme());
		sortCombinations();
		generateFrom( new Combination<Player>(), 0 );
		
		super.endStadistics();
		System.out.println("Tiempo: "+time()+", Iteraciones: "+iteraciones+", Cambio de solucion: "+cambioDeSoluciones);
		
		return incumbent;
	}
	
	private void sortCombinations() 
	{
		for(ArrayList<Combination<Player>> column : combinations) 
			column.sort(PlayerComparator.combinationLevel());
	}
	
	private void generateFrom(Combination<Player> players, int k) 
	{
		iteraciones++;		

		if(players.size() == alignment.numOfPlayers())
		{
				incumbent = new Formation(players.get(), incumbent.getScheme());
				return;
		}
		
		if(k == combinations.size()) 
			return;
		
		for(int i=0; i<combinations.get(k).size(); i++)
		{
			Combination<Player> currentCombination = combinations.get(k).get(i);
			Combination<Player> union  = Combination.union(players, currentCombination);
			if(!inconsistencies.inTeam(union.get()))
				generateFrom(union, k+1);
		}
	}

}
