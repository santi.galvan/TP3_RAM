package business;

import java.util.ArrayList;

public class Squad
{
	private ArrayList<Player> squad;
	
	public Squad()
	{
		squad = new ArrayList<>();
	}
	
	public boolean addPlayer(Player p)
	{
		if(p == null) 
			throw new IllegalArgumentException();
		
		if(squad.size() < 22)
			return squad.add(p);
		
		if(squad.contains(p))
			return false;
		
		return false;
	}

	public int numberOfplayers() 
	{
		return squad.size();
	}
	
	public ArrayList<Player> squad()
	{
		return squad;
	}
	
	public Player getPlayer(int i) 
	{
		return squad.get(i);
	}
	
	public Player getPlayerByName(String playerName) 
	{
		for(Player p : squad)
		{
			if(p.getName().equals(playerName))
				return p;
		}
		return null;
	}

	@Override 
	public String toString() 
	{
		String ret = "";
		for(Player p: squad) 
			ret += p.toString()+", \n";

		return ret;
	}

	public boolean contain(Player player) 
	{
		return squad.contains(player);
	}
	
	public Squad clone()
	{
		Squad ret = new Squad();
		for(Player player: squad)
			ret.addPlayer(player);
		
		return ret;
	}
	
	public ArrayList<Player> players()
	{
		ArrayList<Player> players = new ArrayList<>();
		for(Player p : squad)
		{
			players.add(p.clone());
		}
		return players;
	}
	//¿Esta bien que sean siempre 4 arreglos ? goalkeepers, defenders, midfielders, strikers.
	public ArrayList<ArrayList<Player>> separateSquadByPosition() 
	{
	
		ArrayList<ArrayList<Player>>  ret = new ArrayList<ArrayList<Player>>();
		
		ArrayList<Player> goalkeepers = new ArrayList<Player>();
		squad.stream().filter(e -> e.isGoalKeeper()).forEach(goalkeepers::add);
		
		ArrayList<Player> defenders = new ArrayList<Player>();
		squad.stream().filter(e -> e.isDefender()).forEach(defenders::add);
		
		ArrayList<Player> midfielders = new ArrayList<Player>();
		squad.stream().filter(e -> e.isMidFielder()).forEach(midfielders::add);
		
		ArrayList<Player> strikers = new ArrayList<Player>();
		squad.stream().filter(e -> e.isStriker()).forEach(strikers::add);
		
		ret.add(goalkeepers);
		ret.add(defenders);
		ret.add(midfielders);
		ret.add(strikers);
		
		return ret;
	}

}
