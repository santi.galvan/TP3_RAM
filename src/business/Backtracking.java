package business;

import java.util.ArrayList;

public class Backtracking extends Solver
{
	private ArrayList<ArrayList<Combination<Player>>> combinations;
	
	public Backtracking(Squad instance, Incongruity inconsistencies, String schemeSystem) 
	{
		super.instance = instance;
		super.inconsistencies = inconsistencies;
		super.alignment = new Scheme(schemeSystem); 
	}
	
	@Override
	public Formation solve() 
	{
		super.startStadistics();	
		
		incumbent = new Formation(alignment);
		
		backTracking();
		
		super.endStadistics();
		System.out.println("Tiempo: "+time()+", Iteraciones: "+iteraciones+", Cambio de solucion: "+cambioDeSoluciones);
		
		return incumbent;
	}
	
	private void backTracking()
	{
		this.combinations = CombinationAlgorithm.combinationsTakenFrom(instance.separateSquadByPosition(), alignment.getScheme());
		generateFrom(new Combination<Player>(), 0);
	}
	
	private void generateFrom(Combination<Player> players, int k) 
	{
		iteraciones++;		

		if(players.size() == alignment.numOfPlayers())
		{
			cambioDeSoluciones++;
			if(betterThanIncumbent(players))
			{
				incumbent = new Formation(players.get(), incumbent.getScheme());
				return;
			}
		}
		if(k == combinations.size()) 
			return;
		
		for(int i=0; i<combinations.get(k).size(); i++)
		{
			Combination<Player> currentCombination = combinations.get(k).get(i);
			Combination<Player> union  = Combination.union(players, currentCombination);
			if(!inconsistencies.inTeam(union.get()))
				generateFrom(union, k+1);
		}
	}
	
	private boolean betterThanIncumbent(Combination<Player> players) {
		return Utilities.isBetter(players, incumbent);
	}
	
	public Formation getIncumbent()
	{
		return super.incumbent;
	}
	
	@Override
	public void run()
	{
		solve();
	}
}

