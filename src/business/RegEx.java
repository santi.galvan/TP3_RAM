package business;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import business.Player.Position;

public class RegEx 
{

	public static ArrayList<Integer> createArray(String source) {
		Pattern pattern = Pattern.compile("\\d+"); //Solo numeros separados por comas
		ArrayList<Integer> ret = new ArrayList<>();
		Matcher matcherNumbers = pattern.matcher(source);
		
		while(matcherNumbers.find()) {
			ret.add(Integer.valueOf(matcherNumbers.group()));
		}
		return ret;
	
	}
	
	public static Player[] incongruity(String source) 
	{
		Pattern pattern = Pattern.compile("\\-?\\w(\\.?(\\s?\\w+))*");//Nombres con puntos intermedios y espacios & pesos
		ArrayList<String> mid = new ArrayList<>();
		Matcher matcherPlayer = pattern.matcher(source);
		
		while(matcherPlayer.find()) {
			mid.add(matcherPlayer.group());
		}
		
		Player p1 = new Player(mid.get(0), Double.valueOf(mid.get(1)), Position.valueOf(mid.get(2)));
		Player p2 = new Player(mid.get(3), Double.valueOf(mid.get(4)), Position.valueOf(mid.get(5)));
		
		Incongruity ret = new Incongruity();
		ret.addIncongruity(p1, p2);
		
		return new Player[]{ p1, p2 };
	}
	
	//No admite tildes o el caracter "�", revisar.
	public static Player player(String source) 
	{
		Pattern pattern = Pattern.compile("\\-?\\w(\\.?(\\s?\\w+))*");//Nombres con puntos intermedios y espacios & pesos
		ArrayList<String> mid = new ArrayList<>();
		Matcher matcherPlayer = pattern.matcher(source);
		
		while(matcherPlayer.find()) {
			mid.add(matcherPlayer.group());
		}
		return new Player(mid.get(0), Double.valueOf(mid.get(1)), Position.valueOf(mid.get(2)));
	}
	//Posible factor comun de los dos metodos createFormation y createSquad
	/*
	 * public static ArrayList<Player> players(String[] source)
	 * 	ArrayList<Player> players = new ArrayList<>();
	 * for(String s : source)
	 * 		players.add(playerRegEx);
	 * return players;
	 * 
	 * Y en el codigo de arriba ir�a
	 * Players p = players(source)
	 * Squad s.loadSquad(p);
	 * Formacion f ("4-3-3", players) 
	 * 
	 */
	
}
