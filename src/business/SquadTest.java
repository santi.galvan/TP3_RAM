package business;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.Test;

import business.Player.Position;

public class SquadTest {

	@Test(expected = IllegalArgumentException.class)
	public void addNullPlayer() 
	{
		Squad squad = new Squad();
		assertTrue(squad.addPlayer(null));
	}
	
	@Test
	public void happyPathTest() 
	{
		Player Messi = new Player("Lionel Messi", 10, Position.Striker);
		Player DiMaria = new Player("�ngel Di Maria", 8, Position.Striker);
	
		Squad squad = new Squad();

		assertTrue(squad.addPlayer(Messi));
		assertTrue(squad.addPlayer(DiMaria));
	}
	
	@Test
	public void overflowSquad() // squad.size() < 22
	{
		Squad squad = Utilities.createSquad(new String[] { 
				"Guzman     :  8  (GoalKeeper)",
				"Romero     :  9  (GoalKeeper)", 
			    "Marchesin  :  4  (GoalKeeper)",
				
			    "Fazio	    :  5  (CentralDefender)", 
				"Mammana    :  7  (LeftBack)", 
				"Pezzela    :  8  (RightBack)",
				"Mascherano :  9  (LeftBack)", 
				"Otamendi   :  7  (CentralDefender)",
				"Mercado    :  8  (CentralDefender)", 
				
				"Acuna      :  6  (RightWinger)",
				"Di Maria   :  7  (LeftWinger)",
				"Biglia     :  6  (RightWinger)", 
				"Paredes    :  7  (MidFielder)", 
				"Banega     :  8  (MidFielder)",
				"P.Perez    :  7  (MidFielder)", 
				"I.Fernandez:  7  (MidFielder)",
				"Gago       :  8  (MidFielder)",
				"E.Perez    :  9  (RightWinger)",
				"G.Martinez :  9  (RightWinger)",
				
				"Messi      :  10 (Striker)",
				"Benedeto   :  7  (Striker)", 
				"Icardi     :  8  (Striker)", 
				"Dybala     :  9  (Striker)"});
		
		Player lavezzi = new Player("E.Lavezzi", 0, Position.Striker);
		assertFalse(squad.addPlayer(lavezzi));
	}
	
	// Test de mas es un getter
	@Test
	public void squadTest()
	{
		Squad obtained = Utilities.createSquad(new String[] { 
				"Guzman     :  8  (GoalKeeper)",
				"Icardi     :  8  (Striker)", 
				"Dybala     :  9  (Striker)"});
		
	
		Squad expected = Utilities.createSquad(new String[] { 
				"Guzman     :  8  (GoalKeeper)",
				"Icardi     :  8  (Striker)", 
				"Dybala     :  9  (Striker)"});
	
		Utilities.sameSquad(expected, obtained);
	}
	
	@Test
	public void separateByPosition()
	{
		Squad squad = Utilities.createSquad(new String[] { 
				"Guzman     :  8  (GoalKeeper)",
				
			    "Fazio	    :  5  (CentralDefender)", 
				"Mammana    :  7  (LeftBack)", 
				
				"Acuna      :  6  (RightWinger)",
				
				"Messi      :  10 (Striker)",
				"Benedeto   :  7  (Striker)", 
				"Icardi     :  8  (Striker)"}); 
		
		ArrayList<ArrayList<Player>> expected = new ArrayList<ArrayList<Player>>();
		
		ArrayList<Player> goalkeepers = new ArrayList<Player>();
		ArrayList<Player> defenders = new ArrayList<Player>();
		ArrayList<Player> midfielders = new ArrayList<Player>();
		ArrayList<Player> strikers = new ArrayList<Player>();
		goalkeepers.add(squad.getPlayer(0));
		defenders.add(squad.getPlayer(1));
		defenders.add(squad.getPlayer(2));
		midfielders.add(squad.getPlayer(3));
		strikers.add(squad.getPlayer(4));
		strikers.add(squad.getPlayer(5));
		strikers.add(squad.getPlayer(6));
		expected.add(goalkeepers);
		expected.add(defenders);
		expected.add(midfielders);
		expected.add(strikers);
		
		ArrayList<ArrayList<Player>> obtained = squad.separateSquadByPosition();
		
		assertEquals(expected, obtained);
	}
}
