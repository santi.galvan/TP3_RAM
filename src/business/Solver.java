package business;

public abstract class Solver implements Runnable
{
	Squad instance;
	Incongruity inconsistencies;
	Formation incumbent;
	Scheme alignment;	
	
	//Estadisticas
	long startTime;
	long endTime;
	int iteraciones;
	int cambioDeSoluciones;
	
	abstract Formation solve();
	
	@Override
	public void run()
	{
		solve();
	}
	
	public Formation getIncumbent()
	{
		return incumbent;
	}
	
	void startStadistics() 
	{
		cambioDeSoluciones=0;
		iteraciones =0;
		startTime = System.currentTimeMillis();
	}
	
	void endStadistics() 
	{
		endTime = System.currentTimeMillis();
	}

	double time()
	{
		return (endTime - startTime) / 1000.0;
	}
}
