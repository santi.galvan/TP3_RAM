package business;

import java.io.File;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

public class XmlSave {

	static public void saveSquad(Squad s) 
	{
		
	try
	{
	DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
	DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
	Document doc = docBuilder.newDocument();
	
	Element rootElement = doc.createElement("Squad");
	doc.appendChild(rootElement);
	
	for(Player player : s.players())
	{
		Element e = doc.createElement("Player");
		e.setTextContent(player.toString());
		
		rootElement.appendChild(e);
	}
	
	TransformerFactory transformerFactory = TransformerFactory.newInstance();
	Transformer transformer = transformerFactory.newTransformer();
	
	DOMSource source = new DOMSource(doc);
	StreamResult result = new StreamResult(new File(
		"C:\\xml.prueba"));
	
	transformer.transform(source, result);
	System.out.println("SE guardo el archivo");
	
	
	} catch (ParserConfigurationException pce) {
	    pce.printStackTrace();
	} catch (TransformerException tfe) {
	    tfe.printStackTrace();}
	}
	
	static public void loadSquad(String path)
	{
		try 
		{
		DocumentBuilder builder =
		DocumentBuilderFactory.newInstance().newDocumentBuilder();
		File f = new File(path);
		
		Document documento = builder.parse(f);
		
		
		NodeList hijos = documento.getChildNodes();
	
	
		System.out.println(hijos.item(0).getTextContent());
	
		
		
		}catch(Exception e) {
			System.out.println(e);}
		
	}
	
}
