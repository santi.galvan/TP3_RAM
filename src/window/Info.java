package window;


import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;

import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.ImageIcon;

public class Info extends JDialog {

	private static final long serialVersionUID = 1L;
	private JLabel fondo;
	private JLabel bAccept;

	public Info(JFrame frmWorldCupAplication, boolean modal)
	{
		super(frmWorldCupAplication, modal);
		setTitle("Información");
		setResizable(false);
		setUndecorated(true);
		Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		setBounds((int)screenSize.getWidth()/6+320, (int) screenSize.getHeight()/6+100, 440, 570);
		getContentPane().setLayout(null);
		
		acceptButton();
		fondo();
		
	}

	private void fondo()
	{
		fondo = new JLabel();
		fondo.setIcon(new ImageIcon(Info.class.getResource("/image/infoUtil.png")));
		fondo.setBounds(-7, 0, 447, 586);
		getContentPane().add(fondo);
	}

	private void acceptButton() 
	{
		bAccept = new JLabel("");
		bAccept.setIcon(new ImageIcon(Info.class.getResource("/image/bOkOff.png")));
		bAccept.setBounds(getWidth()/2 - 45, 431, 102, 102);
		bAccept.addMouseListener(new MouseAdapter() 
		{
			@Override
			public void mouseClicked(MouseEvent e)
			{
				dispose();
			}
			@Override
			public void mouseEntered(MouseEvent e) 
			{
				bAccept.setIcon(new ImageIcon(Info.class.getResource("/image/bOkOn.png")));
				
			}
			@Override
			public void mouseExited(MouseEvent e) 
			{
				bAccept.setIcon(new ImageIcon(Info.class.getResource("/image/bOkOff.png")));
			}
		});
		getContentPane().add(bAccept);
	}
}
