package window;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.ImageIcon;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.swing.JTextField;
import java.awt.Font;
import java.awt.Toolkit;
import java.awt.Color;
import java.awt.Dimension;

import javax.swing.SwingConstants;

public class MainForm {

	private JFrame frmWorldCup;
	
	private JLabel fondo;
	private JLabel escudo;
	
	private JLabel bGuardar;
	
	private JLabel lblGk;
	
	private TeamWindow teamwindow;

	private JTextField txtFormacion;
	private JTextField txtResultado;
	private double nivelFormacion; 
	
	private ArrayList<String> formacionSolucion;
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MainForm window = new MainForm();
					window.frmWorldCup.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public MainForm() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		teamwindow = new TeamWindow(frmWorldCup, true);
		Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		
		frmWorldCup = new JFrame();
		frmWorldCup.setTitle("World Cup");
		frmWorldCup.setResizable(false);
		frmWorldCup.setBounds((int)screenSize.getWidth()/6, (int) screenSize.getHeight()/6, 1024, 768);
		frmWorldCup.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmWorldCup.getContentPane().setLayout(null);
		
		teamwindow.setVisible(true);
		
		if(teamwindow.getOperation().equals("accept"))
		{
			formacionSolucion = teamwindow.obtenerResultado();
			dibujarFormacion();
			dibujarPlantel();
			textoResultado();
		}
		
		botonGuardar();
		
		dibujarEscudo();
		textoFormacion();
		fondo();
	}

	private void fondo() {
		fondo = new JLabel("");
		fondo.setIcon(new ImageIcon(MainForm.class.getResource("/image/Fondo.png")));
		fondo.setBounds(-10, 0, 1019, 760);
		frmWorldCup.getContentPane().add(fondo);
	}

	private void textoResultado() 
	{
		String nivel = String.valueOf(nivelFormacion);

		txtResultado = new JTextField();
		txtResultado.setEditable(false);
		txtResultado.setHorizontalAlignment(SwingConstants.LEFT);
		txtResultado.setForeground(Color.GREEN);
		txtResultado.setOpaque(false);
		txtResultado.setBorder(null);
		txtResultado.setText("Nivel:       "+ nivel);
		txtResultado.setBounds(600, 655, 343, 23);
		txtResultado.setFont(new Font("Myriad Hebrew", Font.PLAIN, 25));
		frmWorldCup.getContentPane().add(txtResultado);
		txtResultado.setColumns(10);
	}
	
	private void textoFormacion() 
	{
		txtFormacion = new JTextField();
		txtFormacion.setEditable(false);
		txtFormacion.setHorizontalAlignment(SwingConstants.LEFT);
		txtFormacion.setForeground(Color.WHITE);
		txtFormacion.setOpaque(false);
		txtFormacion.setBorder(null);
		txtFormacion.setText("Formaci\u00F3n");
		txtFormacion.setBounds(126, 43, 445, 62);
		txtFormacion.setFont(new Font("Myriad Hebrew", Font.PLAIN, 50));
		frmWorldCup.getContentPane().add(txtFormacion);
		txtFormacion.setColumns(10);
	}

	private void botonGuardar() {
		bGuardar = new JLabel("");
		bGuardar.setIcon(new ImageIcon(MainForm.class.getResource("/image/bGuardarOff.png")));
		bGuardar.setBounds(895, 24, 102, 102);
		bGuardar.addMouseListener(new MouseAdapter() 
		{
			@Override
			public void mouseEntered(MouseEvent arg0) 
			{
				bGuardar.setIcon(new ImageIcon(MainForm.class.getResource("/image/bGuardarOn.png")));
			}
			@Override
			public void mouseExited(MouseEvent e) 
			{
				bGuardar.setIcon(new ImageIcon(MainForm.class.getResource("/image/bGuardarOff.png")));
			}
		});
		frmWorldCup.getContentPane().add(bGuardar);
	}

	private void dibujarEscudo() 
	{
		escudo = new JLabel();
		escudo.setOpaque(false);
		escudo.setIcon(new ImageIcon(TeamWindow.class.getResource("/teamShield/escudo_default.png")));
		escudo.setBounds(15, -50, 225, 225);
		frmWorldCup.getContentPane().add(escudo);
	}
	
	private void dibujarPlantel() 
	{
		ArrayList<String> arquero = new ArrayList<>();
		formacionSolucion.stream().filter(s -> s.contains("GoalKeeper")).forEach(s -> arquero.add(s));
		
		ArrayList<String> defensores = new ArrayList<>();
		formacionSolucion.stream().filter( s -> s.contains("CentralDefender") || s.contains("RightBack") || s.contains("LeftBack")).forEach(s -> defensores.add(s));
		
		ArrayList<String> mediocampistas = new ArrayList<>();
		formacionSolucion.stream().filter( s -> s.contains("MidFielder") || s.contains("RightWinger") || s.contains("LeftWinger")).forEach(s -> mediocampistas.add(s));
		
		ArrayList<String> delanteros = new ArrayList<>();
		formacionSolucion.stream().filter( s -> s.contains("Striker")).forEach(s -> delanteros.add(s));
		
		int posY = 150;
		int posX = 480;
		int agregadoY = 0;
		//Agrego espacio entre cada uno de ellos, 55 pixeles 
		dibujarEnLista(delanteros, new int[] { posX, posY + agregadoY});		
		agregadoY += delanteros.size()*45; 
		
		dibujarEnLista(mediocampistas, new int[] {posX, posY + agregadoY});
		agregadoY += mediocampistas.size()*45;

		dibujarEnLista(defensores, new int[] {posX, posY + agregadoY});
		agregadoY += defensores.size()*45;
		
		dibujarEnLista(arquero, new int[] {posX, posY + agregadoY});
	}
	
	private void dibujarEnLista(ArrayList<String> players, int[] tuplaXY) 
	{
		int agregadoY = 0;
		for(int i=0; i<players.size(); i++) 
		{
			dibujarDatosJugador(players.get(i), new int[] { tuplaXY[0], tuplaXY[1]+agregadoY } );
			agregadoY += 45;
		}
	
	}
	
	private void dibujarDatosJugador(String datosJugador, int[] tuplaXY) 
	{
		//datos[0] = nombre, datos[1] = nivel, datos[2] = posicion
		ArrayList<String> datos = parseo(datosJugador);
		String posGlobal = asignarPosicionGlobal(datos.get(2));
		String ruta = iconoPosicion(posGlobal);
		String inicialesPos = asignarInicialesPosicion(datos.get(2));
		
		
		lblGk = new JLabel("");
		lblGk.setHorizontalAlignment(SwingConstants.CENTER);
		lblGk.setForeground(Color.WHITE);
		lblGk.setFont(new Font("Myriad Hebrew", Font.PLAIN, 22));
		lblGk.setIcon(new ImageIcon(MainForm.class.getResource(ruta)));
		lblGk.setBounds(tuplaXY[0], tuplaXY[1], 47, 24);
		textoJugadorLista(datos.get(0), Double.valueOf(datos.get(1)), inicialesPos, tuplaXY);
		
		frmWorldCup.getContentPane().add(lblGk);
	}
	
	private void textoJugadorLista(String nombre, double puntaje, String posicion, int[]pos) 
	{
		JTextField nmbLbl = new JTextField(nombre);
		nmbLbl.setForeground(Color.WHITE);
		nmbLbl.setBorder(null);
		nmbLbl.setEditable(false);
		nmbLbl.setFont(new Font("Myriad Hebrew", Font.PLAIN, 17));
		nmbLbl.setBounds(pos[0] + 55, pos[1]+3, 150, 23);
		nmbLbl.setOpaque(false);
		nmbLbl.setColumns(10);
		frmWorldCup.getContentPane().add(nmbLbl);
		
		JTextField posLbl = new JTextField(posicion);
		posLbl.setForeground(Color.WHITE);
		posLbl.setBorder(null);
		posLbl.setEditable(false);
		posLbl.setFont(new Font("Myriad Hebrew", Font.PLAIN, 17));
		posLbl.setBounds(pos[0] + 14 , pos[1]+3, 47, 23);
		posLbl.setOpaque(false);
		posLbl.setColumns(10);
		frmWorldCup.getContentPane().add(posLbl);
		
		nivelFormacion += puntaje; // OJO!!
		JTextField puntLbl = new JTextField(String.valueOf(puntaje));
		puntLbl.setForeground(Color.WHITE);
		puntLbl.setBorder(null);
		puntLbl.setEditable(false);
		puntLbl.setFont(new Font("Myriad Hebrew", Font.PLAIN, 20));
		puntLbl.setBounds(pos[0]+220, pos[1]+3, 47, 23);
		puntLbl.setOpaque(false);
		puntLbl.setColumns(10);
		frmWorldCup.getContentPane().add(puntLbl);
	}
	

	/**METODO PARA DIBUJAR JUGADORES EN LA CANCHA*/
	private void dibujarFormacion() 
	{
		ArrayList<String> arquero = new ArrayList<>();
		formacionSolucion.stream().filter(s -> s.contains("GoalKeeper")).forEach(s -> arquero.add(s));
		
		ArrayList<String> defensores = new ArrayList<>();
		formacionSolucion.stream().filter( s -> s.contains("CentralDefender") || s.contains("RightBack") || s.contains("LeftBack")).forEach(s -> defensores.add(s));
		
		ArrayList<String> mediocampistas = new ArrayList<>();
		formacionSolucion.stream().filter( s -> s.contains("MidFielder") || s.contains("RightWinger") || s.contains("LeftWinger")).forEach(s -> mediocampistas.add(s));
		
		
		ArrayList<String> delanteros = new ArrayList<>();
		formacionSolucion.stream().filter( s -> s.contains("Striker")).forEach(s -> delanteros.add(s));
		
		//cambiar nombres
		dibujar(arquero, new int[] { 205, 610 });
		int[] tuplaXY = new int[2];
		
		tuplaXY = asignarTuplaPara(defensores, "defensor");
		dibujar(defensores, tuplaXY);
		
		tuplaXY = asignarTuplaPara(mediocampistas, "mediocampo");
		dibujar(mediocampistas, tuplaXY);
		
		tuplaXY = asignarTuplaPara(delanteros, "delantero");
		dibujar(delanteros, tuplaXY);
	}

	private int[] asignarTuplaPara(ArrayList<String> jugadores, String posicionGlobal) 
	{		
		int pos = agregadoPosicion(posicionGlobal);
		
		if(jugadores.size() == 1)
			return new int[] { 205, 130 + pos };
		
		if(jugadores.size() <= 4 && jugadores.size() > 1)
			return new int[] { 70, 130 + pos };
		
		if(jugadores.size() <= 6 && jugadores.size() > 4)
			return new int[] { 70, 130 + pos };
		
		if(jugadores.size() <= 8 && jugadores.size() > 6)
			return new int[] { 70, 130 + pos };
		
		throw new RuntimeException("Error al asignar tuplas en " + jugadores);
	}
	
	private int agregadoPosicion(String posicionGlobal) 
	{
		if(posicionGlobal.equals("defensor"))
			return 365;
		
		if(posicionGlobal.equals("mediocampo"))
			return 220;
		
		if(posicionGlobal.equals("delantero"))
			return 80;
		
		throw new RuntimeException("Error al asignar valor en agregado posicion con " + posicionGlobal);
	}
	
	private void dibujar(ArrayList<String> jugadores, int[] tupla) 
	{
		int[] plusX = asignarCombinacionesX(jugadores.size());
		int[] plusY = asignarCombinacionesY(jugadores.size());
		
		for(int i=0; i<jugadores.size(); i++)
			dibujarJugador(jugadores.get(i), new int[] { tupla[0]+plusX[i], tupla[1]+plusY[i] } );
	}
	
	private void dibujarJugador(String datosJugador, int[] tuplaXY) 
	{
		//datos[0] = nombre, datos[1] = nivel, datos[2] = posicion
		ArrayList<String> datos = parseo(datosJugador);
		String posGlobal = asignarPosicionGlobal(datos.get(2));
		String ruta = iconoPosicion(posGlobal);
		String inicialesPos = asignarInicialesPosicion(datos.get(2));

		lblGk = new JLabel("");
		lblGk.setHorizontalAlignment(SwingConstants.CENTER);
		lblGk.setIcon(new ImageIcon(MainForm.class.getResource(ruta)));
		lblGk.setForeground(Color.WHITE);
		lblGk.setFont(new Font("Myriad Hebrew", Font.PLAIN, 22));
		lblGk.setBounds(tuplaXY[0], tuplaXY[1], 47, 24);
		textoJugador(datos.get(0), Double.valueOf(datos.get(1)), inicialesPos, tuplaXY);
		
		frmWorldCup.getContentPane().add(lblGk);
	}
	
	private int[] asignarCombinacionesX(int cantJugadores) 
	{
		if( cantJugadores == 1 ) return new int[] {0};
		
		if( cantJugadores == 2 ) return new int[] {40, 225};
	
		if( cantJugadores == 3 ) return new int[] {0, 135, 275};
		
		if( cantJugadores == 4 ) return new int[] {0, 85, 185, 275};
		
		if( cantJugadores == 5 ) return new int[] {0, 85, 145, 185, 275};
		
		if( cantJugadores == 6 ) return new int[] {0, 85, 85, 185, 185, 275};
		
		if( cantJugadores == 7 ) return new int[] {0, 0, 80, 135, 195, 275, 275};
		
		if( cantJugadores == 8 ) return new int[] {0, 0, 80, 90, 195, 185, 275, 275};

		throw new RuntimeException("Error al asignar combinaciones de X con "+cantJugadores);
	}
	
	private int[] asignarCombinacionesY(int cantJugadores) 
	{
		if( cantJugadores == 1 ) return new int[] {0};
		
		if( cantJugadores == 2 ) return new int[] {0, 0};
		
		if( cantJugadores == 3 ) return new int[] {0, 50, 0};
		
		if( cantJugadores == 4 ) return new int[] {0, 50, 50, 0};
		
		if( cantJugadores == 5 ) return new int[] {0, 50, 0, 50, 0};
		
		if( cantJugadores == 6 ) return new int[] {0, 55, 5, 55, 5, 0};
		
		if( cantJugadores == 7 ) return new int[] {-20, 50, 50, 0, 50, 50, -20};
		
		if( cantJugadores == 8 ) return new int[] {-30, 20, 0, 50, 0, 50, -30, 20};

		throw new RuntimeException("Error al asignar combinaciones de X con "+cantJugadores);
	}
	
	private String asignarInicialesPosicion(String posicion) 
	{
		String ret = "";
		
		if(posicion.equals("Striker")) return "ST"; //caso borde
		
		for (int i = 0; i < posicion.length(); i++) {
			if(Character.isUpperCase(posicion.charAt(i)))
				ret += posicion.charAt(i);
		}
		return ret;
	}
	
	private String asignarPosicionGlobal(String posicion) 
	{
		if(posicion.equals("GoalKeeper")) return "arquero";
		if(posicion.equals("CentralDefender") || posicion.equals("RightBack") || posicion.equals("LeftBack")) return "defensor";
		if(posicion.equals("MidFielder") || posicion.equals("RightWinger") || posicion.equals("LeftWinger")) return "mediocampo";
		if(posicion.equals("Striker")) return "delantero";
		
		throw new RuntimeException("Error al asignar posicion global con " + posicion);
	}
	
	private ArrayList<String> parseo(String player) 
	{
		Pattern pattern = Pattern.compile("\\-?\\w(\\.?(\\s?\\w+))*");//Nombres con puntos intermedios y espacios & pesos
		ArrayList<String> ret = new ArrayList<>();
		Matcher matcherPlayer = pattern.matcher(player);
		
		while(matcherPlayer.find()) {
			ret.add(matcherPlayer.group());
		}
		return ret;
	}
	
	private String iconoPosicion(String posicion)
	{
		if(posicion.equals("arquero"))
			return "/image/cArquero.png";
		
		if(posicion.equals("defensor"))
			return "/image/cDefensa.png";
		
		if(posicion.equals("mediocampo"))
			return "/image/cMedioCampo.png";
		
		if(posicion.equals("delantero"))
			return "/image/cDelantero.png";
		
		throw new RuntimeException("Error al cargar imagen de la posicion " + posicion);
	}

	private void textoJugador(String nombre, double puntaje, String posicion, int[]pos) 
	{
		JTextField nmbLbl = new JTextField(nombre);
		nmbLbl.setForeground(Color.WHITE);
		nmbLbl.setBorder(null);
		nmbLbl.setEditable(false);
		nmbLbl.setFont(new Font("Myriad Hebrew", Font.PLAIN, 17));
		nmbLbl.setBounds(pos[0]+3-nombre.length(), pos[1]-19, 150, 23);
		nmbLbl.setOpaque(false);
		nmbLbl.setColumns(10);
		frmWorldCup.getContentPane().add(nmbLbl);
		
		JTextField posLbl = new JTextField(posicion);
		posLbl.setForeground(Color.WHITE);
		posLbl.setBorder(null);
		posLbl.setEditable(false);
		posLbl.setFont(new Font("Myriad Hebrew", Font.PLAIN, 17));
		posLbl.setBounds(pos[0]+14, pos[1]+3, 47, 23);
		posLbl.setOpaque(false);
		posLbl.setColumns(10);
		frmWorldCup.getContentPane().add(posLbl);
		
		int agregadoPuntaje = (String.valueOf(puntaje).length() == 4 ? 1 : 3 ); //Est�tico: Asigna pixeles para que quede el texto centrado
		JTextField puntLbl = new JTextField(String.valueOf(puntaje));
		puntLbl.setForeground(Color.WHITE);
		puntLbl.setBorder(null);
		puntLbl.setEditable(false);
		puntLbl.setFont(new Font("Myriad Hebrew", Font.PLAIN, 20));
		puntLbl.setBounds(pos[0]+8+agregadoPuntaje, pos[1]+23, 47, 23);
		puntLbl.setOpaque(false);
		puntLbl.setColumns(10);
		frmWorldCup.getContentPane().add(puntLbl);
	}
}